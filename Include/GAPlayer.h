#pragma once
#include "Game.h"
#include <vector>

// A Genetic Algorithm for playing a super simple game
// More for demonstrating the training process than actually playing

class GAPlayer
{
public:

	struct org {
		std::vector<Game::dir> genome;
		int fitness;

		bool operator<(const org& a) const
		{
			return fitness < a.fitness;
		}
	};

	GAPlayer(Game* parentGame);
	~GAPlayer();

	Game::dir selectMove(int step);
	std::vector<Game::dir> train();

	int getGenomeSize() { return genomeSize; }

protected:
	Game* game;

	void freshPopulation();
	void createBoard(std::vector<std::vector<int>>& board);
	void generationReport(int generation, const std::vector<std::vector<int>>& board);
	void breed(const std::vector<org> &prevGeneration);
	void mutate(org& genome);

	const int genomeSize = 8;
	const int maxPopSize = 20;
	const int generations = 10;
	std::vector<org> population;
	std::vector<Game::dir> currentGenome;
};

