#include "Game.h"
#include "GAPlayer.h"
#include <iostream>
#include <string>

Game::Game()
{
	start.x = 4;
	start.y = 4;
	current = start;
	goal.x = 8;
	goal.y = 8;
}


Game::~Game()
{
}

Game::pos Game::move(dir moveDir)
{
	switch (moveDir) {
	case UP:
		current.y++;
		if (current.y >= height) current.y = height - 1;
		break;
	case DOWN:
		current.y--;
		if (current.y < 0) current.y = 0;
		break;
	case LEFT:
		current.x--;
		if (current.x < 0) current.x = 0;
		break;
	case RIGHT:
		current.x++;
		if (current.x >= width) current.x = width - 1;
		break;
	default:
		break;
	}
	return current;
}

Game::pos Game::playout(GAPlayer * player, bool verbose)
{
	// set the starting state
	current = start;
	Game::pos finalPos;
	// Ask the player what moves they want to make
	for (size_t i = 0; i < player->getGenomeSize(); i++)
	{
		if (verbose) std::cout << "Player is at " << current.x << ", " << current.y << std::endl;
		Game::dir curMove = player->selectMove(i);
		finalPos = move(curMove);
		std::string moveString;
		switch (curMove)
		{
		case Game::dir::UP:
			moveString = "Up";
			break;
		case Game::dir::DOWN:
			moveString = "Down";
			break;
		case Game::dir::LEFT:
			moveString = "Left";
			break;
		case Game::dir::RIGHT:
			moveString = "Right";
			break;
		}
		if (verbose) std::cout << "Player has moved " << moveString << " to " << finalPos.x << ", " << finalPos.y << std::endl;
	}
	return finalPos;

}

int Game::distToGoal(pos loc)
{
	return goal.x - loc.x + goal.y - loc.y;
}
