#include "GAPlayer.h"
#include <iostream>
#include <time.h>
#include <algorithm>

GAPlayer::GAPlayer(Game* parentGame)
{
	game = parentGame;
	time_t t;
	srand(time(&t));
}


GAPlayer::~GAPlayer()
{
}

Game::dir GAPlayer::selectMove(int step)
{
	return currentGenome.at(step);
}

std::vector<Game::dir> GAPlayer::train()
{
	// initialise the population
	freshPopulation();

	std::vector<Game::dir> best;

	// Run multiple Generations
	for (size_t g = 0; g < generations; g++)
	{
		// create a 2D vector of the game board size to store our finishing positions
		std::vector<std::vector<int>> results;
		createBoard(results);

		// run every member of the population and receive the results so we can visualise it
		for (size_t i = 0; i < population.size(); i++)
		{
			// run the organism through the entire game and find the end position
			currentGenome = population[i].genome;
			Game::pos finish = game->playout(this, false);
			// ++ the number in the final position
			results[finish.x][finish.y]++;
			// calculate the fitness of each organism (Manhattan Distance)
			population[i].fitness = -game->distToGoal(finish);

			//std::cout << "Org (" << finish.x << ", " << finish.y << ") fitness: " << population[i].fitness << std::endl;
		}
		
		// output the results table
		generationReport(g, results);

		// sort the population based on fitness
		std::sort(population.begin(), population.end());
		
		// grab the best one from this population and save it
		best = population[population.size() - 1].genome;

		// cull the herd (just the lower half of them)
		population.erase(population.begin(), population.begin() + population.size() / 2);

		// breed some more to fill their places, replace old population with a new one
		std::vector<org> oldPop = population;
		breed(oldPop);
	}

	return best;
}

void GAPlayer::freshPopulation()
{
	for (size_t i = 0; i < maxPopSize; i++)
	{
		// create a blank organism
		std::vector<Game::dir> organism;
		for (size_t i = 0; i < genomeSize; i++)
		{	// randomise the directions in it
			organism.push_back((Game::dir)(rand() % Game::dir::NUM_DIR));
		}
		population.push_back({ organism, 0 });
	}
}

void GAPlayer::createBoard(std::vector<std::vector<int>>& board)
{
	// create an empty board and populate it with 0 finishes in every square
	for (size_t i = 0; i < game->getWidth(); i++)
	{
		std::vector<int> column;
		for (size_t j = 0; j < game->getHeight(); j++)
		{
			column.push_back(0);
		}
		board.push_back(column);
	}
}

void GAPlayer::generationReport(int generation, const std::vector<std::vector<int>>& board)
{
	// output the final board state of a generation
	std::cout << "Generation " << generation << std::endl;
	for (int i = game->getHeight() - 1; i >= 0; i--)
	{
		for (size_t j = 0; j < game->getWidth(); j++)
		{
			std::cout << board[j][i] << " ";
		}
		std::cout << std::endl;
	}
}

void GAPlayer::breed(const std::vector<org> &prevGeneration)
{
	// clear current population
	population.clear();

	// possible breeding strategies:
	// 1. Breed pairs with a split point
	// 2. Breed pairs with multiple splits
	// 3. Breed in more than pairs

	for (size_t i = 0; i < prevGeneration.size(); i++)
	{
		// create two pairings for every organism
		int j = rand() % prevGeneration.size();
		while (i == j)
		{	// make sure that i and j aren't the same
			j = rand() % prevGeneration.size();
		}
		int k = rand() % prevGeneration.size();
		while (i == k || j == k)
		{	// make sure that the pairing isn't the same as previous
			k = rand() % prevGeneration.size();
		}

		// Pairs with a single split point
		int split = rand() % genomeSize;
		org newborn;
		newborn.fitness = 0;
		size_t n = 0;
		for (; n <= split; n++)
		{
			newborn.genome.push_back(prevGeneration[i].genome[n]);
		}
		for (; n < genomeSize; n++)
		{
			newborn.genome.push_back(prevGeneration[j].genome[n]);
		}
		// mutate here
		mutate(newborn);
		population.push_back(newborn);

		split = rand() % genomeSize;
		org newborn2;
		newborn2.fitness = 0;
		n = 0;
		for (; n <= split; n++)
		{
			newborn2.genome.push_back(prevGeneration[i].genome[n]);
		}
		for (; n < genomeSize; n++)
		{
			newborn2.genome.push_back(prevGeneration[k].genome[n]);
		}
		mutate(newborn2);
		population.push_back(newborn2);
	}

}

void GAPlayer::mutate(org & genome)
{
	int m = rand() % genomeSize;
	Game::dir d = (Game::dir)(rand() % Game::dir::NUM_DIR);
	genome.genome[m] = d;
}

