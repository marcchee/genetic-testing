#include "LearningApp.h"
#include "GAPlayer.h"
#include <iostream>
#include <string>
#include <vector>

LearningApp::LearningApp()
{
	game = new Game();
	player = new GAPlayer(game);
}


LearningApp::~LearningApp()
{
}

Game::pos LearningApp::run()
{
	Game::pos p;
	return p;
}

void LearningApp::train()
{
	std::vector<Game::dir> solution = player->train();
	std::cout << "Solution is : ";
	for (size_t i = 0; i < solution.size(); i++)
	{
		switch (solution[i])
		{
		case Game::dir::DOWN:
			std::cout << "Down ";
			break;
		case Game::dir::UP:
			std::cout << "Up ";
			break;
		case Game::dir::LEFT:
			std::cout << "Left ";
			break;
		case Game::dir::RIGHT:
			std::cout << "Right ";
			break;
		}
	}
	std::cout << std::endl;
}
